import { defineComponent, PropType } from "vue";
import './Index.scss';
// 类型声明
import { VisualEditorComponent } from './Index.d';

// provide, inject
import { VisualEventBusProvider } from '../../Index.utils';

// 左侧渲染的是注册好的组件
const VisualEditorMenu = defineComponent({
    props: {
        componentList: {
            type: Array as PropType<VisualEditorComponent[]>,
            required: true
        }
    },
    setup(props) {
        const { eventBus } = VisualEventBusProvider.inject();

        // 左侧菜单拖拽
        const menuDraggier = (() => {
            const blockHandler = {
                dragstart: (e: DragEvent, component: VisualEditorComponent) => {
                    // console.log('dragstart');
                    eventBus.emit(component);
                    // dragStart.emit();
                }
            };
            return {
                // dragStart,
                // dragEnd,
                blockHandler
            }
        })();


        return () => <>
            <div class="visual-editor-menu">
                <h3>基础组件</h3>
                <section class="visual-editor-menu-content">
                    {
                        (props.componentList || []).map(component => (
                            <div class="visual-editor-menu-item"
                                draggable
                                onDragstart={(e) => menuDraggier.blockHandler.dragstart(e, component)}
                            >
                                <div class="visual-editor-menu-item-comp">
                                    { component.preview() }
                                </div>
                                <div class="visual-editor-menu-item-text">
                                    { component.label }
                                </div>
                            </div>
                        ))
                    }
                </section>
            </div>
        </>
    }
})

export default VisualEditorMenu;
