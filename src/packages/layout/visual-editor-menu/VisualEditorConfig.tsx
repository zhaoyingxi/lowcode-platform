// 注册左侧菜单组件
import { createVisualEditorConfig } from './Index.utils';
import {
    createEditorColorProp,
    createEditorInputProp,
    createEditorSelectProp,
    createEditorTableProp
} from '../visual-editor-props/Index.utils';
import NumberRange from '../../components/number-range/Index';
import { ElButton, ElInput, ElOption, ElSelect } from 'element-plus';

// 配置
export const VisualConfig = createVisualEditorConfig();

// 注册文本
VisualConfig.registry('text', {
    label: '文本',
    preview: () => '预览文本',
    render: ({ props }) => <span style={{ color: props.color, fontSize: props.size }}>{props.text || '默认文本'}</span>,
    props: {
        text: createEditorInputProp('显示文本'),
        color: createEditorColorProp('字体颜色'),
        size: createEditorSelectProp('字体大小', [
            { label: '14px', val: '14px' },
            { label: '18px', val: '18px' },
            { label: '24px', val: '24px' }
        ])
    }
});

// 注册按钮
VisualConfig.registry('button', {
    label: '按钮',
    preview: () => <ElButton>按钮</ElButton>,
    render: ({ props, size, custom }) => (
        // width height 这么写的原因是：直接用size.width，拖拽改变宽高，然后撤回的时候，没有响应式变化
        <ElButton
            {...custom}
            type={props.type}
            size={props.size}
            style={{
                width: !!size.width ? `${size.width}px` : null,
                height: !!size.height ? `${size.height}px` : null
            }}
        >
            {props.text || '默认按钮'}
        </ElButton>
    ),
    props: {
        text: createEditorInputProp('显示文本'),
        type: createEditorSelectProp('按钮类型', [
            { label: '基础', val: 'primary' },
            { label: '成功', val: 'success' },
            { label: '警告', val: 'warning' },
            { label: '危险', val: 'danger' },
            { label: '提示', val: 'info' },
            { label: '文本', val: 'text' }
        ]),
        size: createEditorSelectProp('按钮大小', [
            { label: '默认', val: '' },
            { label: '中等', val: 'medium' },
            { label: '小', val: 'small' },
            { label: '极小', val: 'mini' }
        ])
    },
    resize: {
        width: true,
        height: true
    }
});

// 注册下拉框
VisualConfig.registry('select', {
    label: '下拉框',
    preview: () => <ElSelect />,
    render: ({ props, model, custom }) => (
        <ElSelect
            {...custom}
            key={(props.options || []).map((opt: any) => opt.value).join(',')}
            {...model.default}
        >
            {
                (props.options || []).map((opt: { label: string, value: string }, index: number) => (
                    <ElOption label={opt.label} value={opt.value} key={index} />
                ))
            }
        </ElSelect>
    ),
    props: {
        options: createEditorTableProp('下拉选项', {
            options: [
                { label: '显示值', field: 'label' },
                { label: '绑定值', field: 'value' },
                { label: '备注', field: 'comments' }
            ],
            showKey: 'label'
        })
    },
    model: {
        default: '绑定字段'
    }
})

// 注册输入框
VisualConfig.registry('input', {
    label: '输入框',
    preview: () => <ElInput modelValue={""} />,
    render: ({ model, size, custom }) => <ElInput
        {...custom}
        {...model.default}
        style={{
            width: !!size.width ? `${size.width}px` : null,
        }} />,
    model: {
        default: '绑定字段'
    },
    resize: {
        width: true
    }
});

// 注册数字范围输入框
VisualConfig.registry('number-range', {
    label: '数字范围输入框',
    preview: () => <NumberRange />,
    render: ({ model, size }) => <NumberRange
        style={{ width: `${size.width}px` }}
        {...{
            start: model.start.value,
            'onUpdate:start': model.start.onChange,
            end: model.end.value,
            'onUpdate:end': model.end.onChange,
        }}
    />,
    model: {
        start: '起始绑定字段',
        end: '结束绑定字段'
    },
    resize: {
        width: true
    }
});

// 注册图片
VisualConfig.registry('image', {
    label: '图片',
    preview: () => <img src="http://cn.vuejs.org/images/logo.png" alt="图片alt" />,
    render: ({ props, size }) => (
        <div
            style={{ height: `${size.height || 100}px`, width: `${size.width || 100}px` }}
            class="visual-block-img">
            <img src={props.url || 'http://cn.vuejs.org/images/logo.png'} />
        </div>
    ),
    props: {
        url: createEditorInputProp('地址')
    },
    resize: {
        width: true,
        height: true
    }
});
