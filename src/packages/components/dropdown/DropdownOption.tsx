import { defineComponent } from 'vue';
import './DropdownOption.scss';
import { DropdownProvider } from './Index.utils';

// 定义下拉组件
const DropdownOption = defineComponent({
    props: {
        label: { type: String },
        icon: { type: String }
    },
    emits: {
        click: (e: MouseEvent) => true
    },
    setup(props, ctx) {
        // 子组件中调用关闭事件
        const { onClick: dropdownClickHandler} = DropdownProvider.inject();
        const handler = {
            onClick: (e: MouseEvent) => {
                ctx.emit('click', e); // 触发绑定的click方法
                dropdownClickHandler(); // 关闭弹窗方法
            }
        }

        return () => (
            <div class="dropdown-option" onClick={handler.onClick}>
                <i class={`iconfont ${props.icon}`}></i>
                <span>{props.label}</span>
            </div>
        )
    }
})

export default DropdownOption;
