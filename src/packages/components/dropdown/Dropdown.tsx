// 右键菜单
import { computed, createApp, defineComponent, getCurrentInstance, onBeforeUnmount, onMounted, PropType, reactive, ref } from "vue";
import './Dropdown.scss';
// 类型声明
import { IDropdownOption } from './Index';
// 工具方法
import { DropdownProvider } from './Index.utils';
import { defer } from "../../utils/defer";


// 弹窗组件，用于右键展示操作选项
const ServiceComponent = defineComponent({
    props: {
        option: {
            type: Object as PropType<IDropdownOption>,
            required: true
        }
    },
    setup(props) {
        // 获取当前组件实例，用于将将service绑定到实例上
        const ctx = getCurrentInstance()!;
        const dropdownRef = ref({} as HTMLDivElement);
        // 响应是状态
        const state = reactive({
            option: props.option,
            showFlag: false,
            top: 0,
            left: 0,
            mounted: (() => {
                const dfd = defer();
                onMounted(() => {
                    setTimeout(() => { // 增加延迟是为了第一次右键也有动画效果
                        dfd.resolve()
                    }, 0)
                });
                return dfd.promise;
            })()
        })
        // 方法
        const methods = {
            show: async () => {
                await state.mounted;
                state.showFlag = true
            },
            hide: async () => {
                state.showFlag = false
            }
        }

        const service = (option: IDropdownOption) => {
            state.option = option; 
            // HTMLElement
            if ('addEventListener' in option.evt) {
                const { top, left, height } = option.evt.getBoundingClientRect()!;
                state.left = left;
                state.top = top + height;
                // MouseEvent
            } else {
                const { clientX, clientY } = option.evt;
                state.left = clientX;
                state.top = clientY;
            }
            methods.show();
        }

        // 计算类名
        const classes = computed(() => [
            'dropdown',
            {
                'dropdown-show': state.showFlag
            }
        ])
        // 计算样式
        const styles = computed(() => ({
            top: `${state.top}px`,
            left: `${state.left}px`
        }))

        // 将service绑定到实例上
        Object.assign(ctx.proxy, { service });

        // 点击block外的区域，则隐藏小弹窗
        const onMousedownDocument = (e: MouseEvent) => {
            if (!(dropdownRef.value).contains(e.target as HTMLElement)) {
                methods.hide();
            }
        }
        onMounted(() => document.body.addEventListener('mousedown', onMousedownDocument, true))
        onBeforeUnmount(() => document.body.removeEventListener('mousedown', onMousedownDocument, true))

        // 父组件向子组件传递关闭方法
        DropdownProvider.provide({ onClick: methods.hide });

        return () => (
            <div class={classes.value} style={styles.value} ref={dropdownRef}>
                {state.option.render()}
            </div>
        )
    }

})

// 下拉组件
const $$dropdown = (() => {
    let instance: any;
    return (option: IDropdownOption) => {
        if (!instance) {
            const el = document.createElement('div');
            document.body.append(el);
            const app = createApp(ServiceComponent, { option });
            instance = app.mount(el);
        }
        instance.service(option);
    }
})();

export default $$dropdown;
