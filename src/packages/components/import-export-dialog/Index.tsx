// 导入导出弹窗
import { createApp, defineComponent, getCurrentInstance, PropType, reactive } from "vue";
import { ElInput, ElDialog, ElButton } from 'element-plus';
// 类型声明
import { DialogServiceOption } from './Index.d';
// 工具函数
import { DialogServiceEditType, keyGenerator } from './Index.utils';
import { defer } from "../../utils/defer";

const ServiceComponent = defineComponent({
    props: {
        option: { type: Object as PropType<DialogServiceOption>, required: true }
    },
    setup(props) {
        // 获取当前实例
        const ctx = getCurrentInstance()!;
        // 响应式变量
        const state = reactive({
            option: props.option,
            editValue: null as undefined | null | string,
            showFlag: false,
            key: keyGenerator()
        });

        const methods = {
            service: (option: DialogServiceOption) => {
                state.option = option;
                state.editValue = option.editValue;
                state.key = keyGenerator();
                methods.show();
            },
            show: () => {
                state.showFlag = true;
            },
            hide: () => {
                state.showFlag = false;
            }
        }

        const handler = {
            onConfirm: () => {
                state.option.onConfirm(state.editValue); // 执行promise的resolve
                methods.hide();
            },
            onCancel: () => {
                methods.hide();
            }
        }

        // 将methods绑定到实例上（否则访问不到）
        Object.assign(ctx.proxy, methods);

        return () => (
            // @ts-ignore
            <ElDialog v-model={state.showFlag} title={state.option.title} key={state.key}>
                {{
                    default: () => (
                        <div>
                            {
                                state.option.editType === DialogServiceEditType.textarea
                                ? ( <ElInput type="textarea" {...{ row: 20 }} v-model={state.editValue} /> )
                                : (<ElInput v-model={state.editValue} /> )
                            }
                        </div>
                    ),
                    footer: () => (
                        <div>
                            <ElButton {...{ onClick: handler.onCancel } as any}>取消</ElButton>
                            <ElButton {...{ onClick: handler.onConfirm } as any}>确定</ElButton>
                        </div>
                    )
                }}
            </ElDialog>
        )
    }
})

// 创建组件并挂在到DOM上
const DialogService = (() => {
    let instance: any;
    return (option: DialogServiceOption) => {
        if (!instance) {
            const el = document.createElement('div');
            document.body.appendChild(el);
            const app = createApp(ServiceComponent, { option });
            instance = app.mount(el);
        }
        instance.service(option);
    }
})()

// 将对象合并到DialogService函数的原型上
const $$dialog = Object.assign(DialogService, {
    input: (initValue?: string, title?: string, option?: Omit<DialogServiceOption, 'editType' | 'onConfirm'>) => {
        const dfd = defer<string | null | undefined>();
        const opt: DialogServiceOption = {
            ...option,
            editType: DialogServiceEditType.input,
            editValue: initValue,
            onConfirm: dfd.resolve,
            title
        };
        DialogService(opt);
        return dfd.promise;
    },
    textarea: (initValue?: string, title?: string, option?: Omit<DialogServiceOption, 'editType' | 'onConfirm'>) => {
        const dfd = defer<string | null | undefined>();
        const opt: DialogServiceOption = {
            ...option,
            editType: DialogServiceEditType.textarea,
            editValue: initValue,
            onConfirm: dfd.resolve,
            title
        };
        DialogService(opt);
        return dfd.promise;
    }
})

export default $$dialog;
