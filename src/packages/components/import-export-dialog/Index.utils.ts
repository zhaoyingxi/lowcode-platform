// 弹窗内部展示的组件类型，文本域或输入框
export enum DialogServiceEditType {
    textarea = 'textarea',
    input = 'input'
}

// 为了每次展示弹窗都是最新的内容，所以加了key
export const keyGenerator = (() => {
    let count = 0;
    return () => `auto_key_${count++}`;
})()