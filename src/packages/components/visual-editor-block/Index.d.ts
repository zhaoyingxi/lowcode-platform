// block类型声明
export interface VisualEditorBlockData {
    componentKey: string; // text, input ...
    width: number; // block的宽度
    height: number;// block的高度
    top: number;  // 位置
    left: number; // 位置
    adjustPosition?: boolean; // 鼠标抬起时是否需要调整位置（为true时，鼠标抬起，组件的中间位置位于鼠标的位置）
    focus?: boolean; // 是否为选中状态
    zIndex: number; // 权重，用于置顶置底
    hasResize?: boolean; // 是否调整过宽度，用于展示block的时候，如果调整过宽度则渲染数据中的width和height
    props: Record<string, any>; // 组件的设计属性
    model: Record<string, string>; // 绑定的字段
    slotName?: string; // slotName -组件唯一标识
}

// 当前操作block的标志线
export interface VisualEditorMarkLines {
    x: { left: number, markX: number }[],
    y: { top: number, markY: number }[]
}
